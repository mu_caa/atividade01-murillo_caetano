/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista_1;

import java.util.Random;

/**
 *
 * @author 140041020582
 */
public class ex06 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] vetorA = criarVetor(10);
        int[] vetorB = criarVetor(10);
        int[] vetorC = criarVetor(10);

        armazenarSomaVetorC(vetorA, vetorB, vetorC);
        imprimirVetor(vetorA, vetorB, vetorC, 1);
    }

    static int[] criarVetor(int tamanho) {

        Random random = new Random();
        int[] vetor = new int[tamanho];
        for (int i = 0; i < 10; i++) {
            vetor[i] = random.nextInt(21);
        }
        return vetor;

    }

    static int[] armazenarSomaVetorC(int[] vetorA, int[] vetorB, int[] vetorC) {
        for (int i = 0; i < vetorC.length; i++) {
            vetorC[i] = vetorA[i] + vetorB[i];
        }
        return vetorC;
    }

    static void imprimirVetor(int[] vetorA, int[] vetorB, int[] vetorC, int vetorNum) {
        if (vetorNum == 1) {
            System.out.println("Vetor A");
            for (int i = 0; i < vetorA.length; i++) {
                
                System.out.print(vetorA[i] + ", ");
            }
        } else if (vetorNum == 2) {
            System.out.println("Vetor B");
            for (int i = 0; i < vetorB.length; i++) {
                System.out.print(vetorB[i] + ", ");
            }
        } else if (vetorNum == 3) {
            System.out.println("Vetor C");
            for (int i = 0; i < vetorC.length; i++) {
                System.out.print(vetorC[i] + ", ");
            }
        }
    }
}
